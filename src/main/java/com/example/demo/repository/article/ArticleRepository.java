package com.example.demo.repository.article;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.repository.provider.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {

    @Insert("Insert into tb_articles(title, description, author, thumbnail, created_date, category_id) " +
            "values(#{title}, #{description}, #{author}, #{thumbnail}, #{createDate}, #{category.id})")
    public void insert(Article article);

    @Select("select a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.category from tb_articles a Inner join tb_categories c On a.category_id = c.id where a.id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.name", column = "category"),
    })
    public Article findOne(int id);

    @Select("select a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.category from tb_articles a Inner join tb_categories c On a.category_id = c.id limit 5")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.name", column = "category"),
    })
    public List<Article> findAll();

    @Delete("delete from tb_articles where id=#{id}")
    public void delete(int id);

    @Update("update tb_articles set title=#{title}, description=#{description}, author=#{author}, category_id=#{category.id} where id=#{id}")
    public void edit(Article article);

    @SelectProvider(method = "findAllFilter", type = ArticleProvider.class)
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.name", column = "category"),
    })
    public List<Article> findAllFilter(ArticleFilter articleFilter);

}
