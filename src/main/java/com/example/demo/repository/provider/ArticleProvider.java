package com.example.demo.repository.provider;

import com.example.demo.model.ArticleFilter;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {

    public String findAllFilter(ArticleFilter articleFilter){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.category");
            FROM("tb_articles a");
            INNER_JOIN("tb_categories c On a.category_id = c.id");
            if(articleFilter.getTitle() != null)
                WHERE("a.title ILIKE '%' || #{title} || '%'");
            if(articleFilter.getCate_id() != null)
                WHERE("a.category_id = #{cate_id}");

            ORDER_BY("a.id ASC");
        }}.toString();
    }

}
