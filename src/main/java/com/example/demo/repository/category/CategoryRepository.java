package com.example.demo.repository.category;

import com.example.demo.model.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface CategoryRepository {

    @Insert("INSERT INTO tb_categories(category) VALUES(#{name})")
    public void insert(Category category);

    @Select("select * from tb_categories")
    @Results({@Result(property = "name", column = "category")})
    public List<Category> findAll();

    @Select("select * from tb_categories where id = #{id}")
    @Results({@Result(property = "name", column = "category")})
    public Category findOne(int id);

    @Delete("delete from tb_categories where id=#{id}")
    public void delete(int id);

    @Update("update tb_categories set category=#{name} where id=#{id}")
    public void update(Category category);

}
