package com.example.demo.service.article;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;

import java.util.List;

public interface ArticleService {

    void insert(Article article);
    Article findOne(int id);
    List<Article> findAll();
    void delete(int id);
    void edit(Article article);
    List<Article> findAllFilter(ArticleFilter articleFilter);

}
