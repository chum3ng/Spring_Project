package com.example.demo.service.article;

import com.example.demo.model.Article;
import com.example.demo.model.ArticleFilter;
import com.example.demo.repository.article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImplement implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public void insert(Article article) {
        articleRepository.insert(article);
    }

    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

    @Override
    public void edit(Article article) {
        articleRepository.edit(article);
    }

    @Override
    public List<Article> findAllFilter(ArticleFilter articleFilter) {
        return articleRepository.findAllFilter(articleFilter);
    }
}
