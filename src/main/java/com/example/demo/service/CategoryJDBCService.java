package com.example.demo.service;

import com.example.demo.repository.CategoryJDBCRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryJDBCService {

    @Autowired
    private CategoryJDBCRepository categoryJDBCRepository;

    public void showAllCategories(){
        categoryJDBCRepository.showAllCategories();
    }

}
