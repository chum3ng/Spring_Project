package com.example.demo.service.category;

import com.example.demo.model.Category;
import com.example.demo.repository.category.CategoryRepository;
import com.github.javafaker.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public void insert(Category category) { categoryRepository.insert(category); }

    public List<Category> findAll(){
        return categoryRepository.findAll();
    }

    public Category findOne(int id){
        return categoryRepository.findOne(id);
    }

    public void update(Category category){ categoryRepository.update(category); }

    public void delete(int id){ categoryRepository.delete(id); }

}
